<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

/** 
 * @param string $product_name Product Name 
 * @param string $optional_parameter Optional for Search Images
 */
function fn_altteam_google_images_get_images($product_name = '', $optional_parameter = '')
{
    $google_console_key = str_replace(' ', '', Registry::get('addons.altteam_google_images.google_image_key'));
    $google_custom_search_key = str_replace(' ', '', Registry::get('addons.altteam_google_images.google_cse_key'));

    if (!empty($google_console_key) && !empty($google_custom_search_key)) {
        if (!empty($optional_parameter)) {
            $json = file_get_contents(
                "https://www.googleapis.com/customsearch/v1?key="
                    . $google_console_key
                    . "&cx="
                    . $google_custom_search_key
                    . "&q="
                    . str_replace(' ', '', $product_name)
                    . "+"
                    . str_replace(' ', '', $optional_parameter)
                    . "&searchType=image"
            );
        } else {
            $json = file_get_contents(
                "https://www.googleapis.com/customsearch/v1?key="
                    . $google_console_key
                    . "&cx="
                    . $google_custom_search_key
                    . "&q="
                    . str_replace(' ', '', $product_name)
                    . "&searchType=image"
            );
        }
        
        $data = json_decode($json, true);

        $img_links = array();

        foreach ($data['items'] as $item) {
            $img_links[] = $item['link'];
        }
        return $img_links;
    }
}
/**
 * @param string $img_link Image link
 * @param int    $object_id Snap to Object
 * @param string $type Type for Image Default product
 * @param string $prefix Prefix for Image imported Default ''
 */
function fn_allteam_google_images_put_img_link($img_link = '', $object_id = 0, $object_type = 'product', $prefix = '')
{
    $main_id = db_get_row('SELECT MAX(image_id),MAX(position) FROM ?:images_links WHERE object_id = ?i and object_type = ?s', $object_id, $object_type);
    $position = (int) $main_id['MAX(position)'];
    if (!empty($main_id)) {
        fn_exim_import_images(
            $prefix,
            $img_link,
            $img_link,
            $position += 1,
            ($main_id['MAX(image_id)'] == 0) ? 'M' : 'A',
            $object_id,
            $object_type
        );
    }
    return;
}
