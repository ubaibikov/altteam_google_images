<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/

use Tygh\Registry;
use Tygh\Settings;

defined('BOOTSTRAP') or die('Access denied');


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'google_img') {
        fn_allteam_google_images_put_img_link($_REQUEST['img_link'],$_REQUEST['product_id']);
    }
}