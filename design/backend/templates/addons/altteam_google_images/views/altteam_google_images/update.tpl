<div class="" title="{__("altteam_price_button.how_is_price")}" id="content_add_google_images">
{if $product_data.product && $product_data.product_code}
    <div class="altteam_google_images">  
        <div class="container">
            <div class="row">
            {foreach from=$product_data.product|fn_altteam_google_images_get_images:$product_data.product_code item="img_link" }
                    <div class="col-sm">
                        <img class="img" src={$img_link} />
                    </div>
            {/foreach}
            </div>
        </div>
    </div>
{/if}
    
<!--content_add_google_images--></div>
<script type="text/javascript">
    $(".img").click(function(){
        var img_link = $(this).attr('src');
        var data = {
            "img_link": img_link,
            "product_id": {$smarty.request.product_id}
        } 
        $.ceAjax('request', fn_url('altteam_google_images.google_img'), {
            method: 'POST',
            data: data,
            callback: function(html){
                location.reload();  
            },
            result_ids: 'last_edited_items',
        }); 
    });
</script>